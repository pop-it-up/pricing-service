const { Schema, model, Types } = require("mongoose");

const RoomSchema = new Schema({
  room: {
    type: Types.ObjectId,
    required: true
  },
  nrOfSeats: {
    type: Number,
    default: 0
  }
});

const Room = model("PricingRoom", RoomSchema);

module.exports = Room;