const { Schema, model, Types } = require("mongoose");

const TimeSlotSchema = new Schema({
  timeSlot: {
    type: Types.ObjectId,
    required: true
  },
  movie: {
    type: Types.ObjectId,
    required: true
  },
  event: {
    type: Types.ObjectId,
    required: true
  },
  room: {
    type: Types.ObjectId,
    required: true
  }
});

const TimeSlot = model("PricingTimeSlot", TimeSlotSchema);

module.exports = TimeSlot;
