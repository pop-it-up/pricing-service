const { Schema, model, Types } = require("mongoose");

const BasePriceSchema = new Schema({
  amount: {
    type: Number,
    default: 0
  },
  event: {
    type: Types.ObjectId,
    required: true
  },
  movie: {
    type: Types.ObjectId,
    required: true
  }
});

const BasePrice = model("BasePrice", BasePriceSchema);

module.exports = BasePrice;
