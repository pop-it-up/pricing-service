const { Schema, model, Types } = require("mongoose");

const MovieSchema = new Schema({
  movie: {
    type: Types.ObjectId,
    required: true
  },
  price: {
    type: Number,
    required: true
  }
});

const Movie = model("PricingMovie", MovieSchema);

module.exports = Movie;
