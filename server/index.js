const bodyParser = require("body-parser");
const app = require("express")();
const db = require("./database.js");

const passport = require("passport");
const { ExtractJwt } = require("passport-jwt");

require("./cors.js")(app);
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("JWT");
jwtOptions.secretOrKey = "somesecret";

require("./jwt.js")(app, passport, jwtOptions);

require("./api")(app);

db.connect();
const port = 80;
app.listen(port, () => {
  console.log("Server running...");
});
