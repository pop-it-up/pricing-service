const { Strategy } = require("passport-jwt");

const axios = require("axios");
const base = "http://localhost:3333/admin-service";

module.exports = function(app, passport, jwtOptions) {
  var strategy = new Strategy(jwtOptions, function(jwt_payload, next) {
    if (!jwt_payload) next(null, false);
    if (!jwt_payload._id) next(null, false);
    // usually this would be a database call:

    axios
      .get(`${base}/admins/${jwt_payload._id}`)
      .then(r => {
        const user = r.data;
        if (user) {
          next(null, user);
        } else {
          next(null, false);
        }
      })
      .catch(e => {
        console.log(e);
        next(null, false);
      });
  });

  passport.use(strategy);
  app.use(passport.initialize());
};
