const BasePrice = require("./models/BasePrice");
const Movie = require("./models/Movie");
require("./implementation/basePrice");

const objectFromList = list =>
  list.reduce(
    (a, c) => ({
      ...a,
      [c._id]: c
    }),
    {}
  );
function api(app) {
  app.get(`/base-prices/`, (req, res) => {
    BasePrice.find(req.query)
      .then(r => {
        Promise.all(r.map(p => Movie.findById(p.movie))).then(movies => {
          const mObj = objectFromList(movies);
          res.json(
            r.map(p => {
              p.movie = mObj[p.movie].movie;
              return p;
            })
          );
        });
      })
      .catch(err => {
        res.status(400).send(err);
      });
  });
}
module.exports = api;
