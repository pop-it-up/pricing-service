const Movie = require("../models/Movie");
const Room = require("../models/Room");
const TimeSlot = require("../models/TimeSlot");
const BasePrice = require("../models/BasePrice");
const redis = require("redis");
const client = redis.createClient({
  host: "queue-redis-master",
  port: 6379
});
function updateBasePrice(event, movie) {
  console.log("update");
  Promise.all([TimeSlot.find({ event, movie }), Movie.findOne({ movie })]).then(
    ([timeslots, movie]) => {
      console.log(timeslots);
      Promise.all(timeslots.map(t => Room.findOne({ room: t.room }))).then(
        rooms => {
          const totalSeats = rooms.reduce((sum, room) => {
            return sum + room.nrOfSeats;
          }, 0);
          console.log(rooms, totalSeats);

          const amount = Math.ceil((movie.price / totalSeats) * 0.7);
          console.log(movie, amount);
          BasePrice.findOne({ event, movie }, (err, price) => {
            if (!err) {
              if (price) {
                price.amount = amount;
                price.save();
              } else {
                const price = new BasePrice({ amount, event, movie });
                price.save();
              }
            } else {
              const price = new BasePrice({ amount, event, movie });
              price.save();
            }
          });
        }
      );
    }
  );
}

const movieSubscriber = {
  onCreate: obj => {
    if (obj._id) {
      obj.movie = obj._id;
      delete obj._id;
    }
    const movie = new Movie(obj);
    movie.save().catch(err => {
      console.log(err);
      setTimeout(() => {
        movieSubscriber.onCreate(obj);
      }, 500);
    });
  },
  onUpdate: obj => {
    if (obj._id) {
      obj.movie = obj._id;
      delete obj._id;
    }
    Movie.findOneAndUpdate({ movie: obj.movie }, { price }).catch(err => {
      console.log(err);
      setTimeout(() => {
        movieSubscriber.onUpdate(obj);
      }, 500);
    });
  },
  onDelete: ({ _id }) => {
    Movie.findOneAndDelete({ movie: _id }).catch(err => {
      console.log(err);
      setTimeout(() => {
        movieSubscriber.onDelete({ _id });
      }, 500);
    });
  }
};

const timeSlotSubscriber = {
  onCreate: obj => {
    if (obj._id) {
      obj.timeSlot = obj._id;
      delete obj._id;
    }
    const timeslot = new TimeSlot(obj);
    timeslot
      .save()
      .then(r => {
        updateBasePrice(r.event, r.movie);
      })
      .catch(err => {
        console.log(err);
        setTimeout(() => {
          timeSlotSubscriber.onCreate(obj);
        }, 500);
      });
  },
  onUpdate: obj => {
    if (obj._id) {
      obj.timeSlot = obj._id;
      delete obj._id;
    }
    TimeSlot.findOneAndUpdate({ timeSlot: obj.timeSlot }, obj)
      .then(r => {
        updateBasePrice(r.event, r.movie);
      })
      .catch(err => {
        console.log(err);
        setTimeout(() => {
          timeSlotSubscriber.onUpdate(obj);
        }, 500);
      });
  },
  onDelete: ({ _id }) => {
    TimeSlot.findOneAndDelete({ timeSlot: _id })
      .then(t => {
        console.log(t);
        updateBasePrice(t.event, t.movie);
      })
      .catch(err => {
        console.log(err);
        setTimeout(() => {
          timeSlotSubscriber.onDelete({ _id });
        }, 500);
      });
  }
};

const roomSubscriber = {
  onCreate: ({ _id }) => {
    console.log({ _id });
    const room = new Room({ room: _id });
    room.save().catch(err => {
      console.log(err);
      setTimeout(() => {
        roomSubscriber.onCreate({ _id });
      }, 500);
    });
  },
  onUpdate: obj => {
    if (obj._id) {
      obj.room = obj._id;
      delete obj._id;
    }
    console.log(obj);
    Room.findOneAndUpdate({ room: obj.room }, obj)
      .then(r => {
        TimeSlot.find({ room: obj.room }).then(r => {
          r.forEach(t => {
            updateBasePrice(t.event, t.movie);
          });
        });
      })
      .catch(err => {
        console.log(err);
        setTimeout(() => {
          roomSubscriber.onUpdate(obj);
        }, 500);
      });
  },
  onDelete: ({ _id }) => {
    Room.findOneAndDelete({ room: _id })
      .then(r => {
        TimeSlot.find({ room: _id }).then(r => {
          r.forEach(t => {
            timeSlotSubscriber.onDelete(t);
          });
        });
      })
      .catch(err => {
        console.log(err);
        setTimeout(() => {
          roomSubscriber.onDelete({ _id });
        }, 500);
      });
  }
};
const seatSubscriber = {
  onCreate: ({ room }) => {
    console.log("room id:", room);
    Room.findOneAndUpdate(
      { room },
      {
        $inc: {
          nrOfSeats: 1
        }
      },
      (err, obj) => {
        console.log("Updated seat number", obj);
        TimeSlot.find({ room: obj.room }).then(r => {
          r.forEach(t => {
            updateBasePrice(t.event, t.movie);
          });
        });
      }
    );
  },
  onUpdate: () => {},
  onDelete: ({ room }) => {
    Room.findOneAndUpdate(
      { room },
      {
        $inc: {
          nrOfSeats: -1
        }
      },
      (err, obj) => {
        TimeSlot.find({ room: obj.room }).then(r => {
          r.forEach(t => {
            updateBasePrice(t.event, t.movie);
          });
        });
      }
    );
  }
};

function messageHandler(message, subscriber) {
  switch (message.type) {
    case "create":
      subscriber.onCreate(message.payload);
      break;
    case "update":
      subscriber.onUpdate(message.payload);
      break;
    case "delete":
      subscriber.onDelete(message.payload);
      break;
    default:
      break;
  }
}
client.on("message", function(channel, message) {
  console.log(channel);
  message = JSON.parse(message);
  switch (channel) {
    case "room":
      messageHandler(message, roomSubscriber);
      break;
    case "movie":
      messageHandler(message, movieSubscriber);
      break;
    case "seat":
      messageHandler(message, seatSubscriber);
      break;
    case "timeSlot":
      messageHandler(message, timeSlotSubscriber);
      break;
    default:
      break;
  }
});

client.subscribe("room", "movie", "seat", "timeSlot");
