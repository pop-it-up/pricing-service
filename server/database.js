const mongoose = require("mongoose");

// connect to Mongo daemon
function connect() {
  mongoose
     .connect(`mongodb://popitup:wearegonnapopit@pricing-mongodb:27017/admin`, {
      useNewUrlParser: true
    })
    .then(() => console.log("MongoDB Connected"))
    .catch(err => {
      console.log(err);
      setTimeout(() => {
        connect();
      }, 100);
    });
}

module.exports = {
  connect
};
