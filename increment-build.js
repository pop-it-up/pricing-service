const fs = require("fs");
require("dotenv").config();
const image = process.env.IMAGE_NAME;

let newImageName = image.split(":");
newImageName[1] = (Number(newImageName[1]) + 0.01).toFixed(2);
newImageName = newImageName.join(":");

const files = [".env", "deployment.yaml", ".rancher-pipeline.yml"];
function replaceImageName(file) {
  fs.readFile(file, "utf8", function(err, data) {
    if (err) {
      return console.log(err);
    }
    var result = data.replace(new RegExp(`${image}`), newImageName);

    fs.writeFile(file, result, "utf8", function(err) {
      if (err) return console.log(err);
    });
  });
}

files.forEach(f => replaceImageName(f));
