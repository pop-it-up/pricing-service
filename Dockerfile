FROM node:10 

WORKDIR /app 

COPY . . 

EXPOSE 80 

RUN npm install -g nodemon 

CMD [ "nodemon", "./server/index.js" ]
